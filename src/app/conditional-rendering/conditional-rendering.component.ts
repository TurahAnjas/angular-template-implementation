import { Component } from '@angular/core';

@Component({
  selector: 'app-conditional-rendering',
  templateUrl: './conditional-rendering.component.html',
  styleUrls: ['./conditional-rendering.component.scss']
})
export class ConditionalRenderingComponent {
  public isLoading: boolean = false;

  constructor() {}

  public hanndleToggleLoading(): void {
    this.isLoading = !this.isLoading;

    setTimeout(() => {
      this.isLoading = !this.isLoading;
    }, 5000);
  }
}
