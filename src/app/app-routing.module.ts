import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntroductionComponent } from './introduction/introduction.component';
import { ReusableTemplateComponent } from './reusable-template/reusable-template.component';
import { ConditionalRenderingComponent } from './conditional-rendering/conditional-rendering.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent,
  },
  {
    path: 'intro',
    component: IntroductionComponent,
  },
  {
    path: 'reusable-template',
    component: ReusableTemplateComponent,
  },
  {
    path: 'conditional-rendering',
    component: ConditionalRenderingComponent,
  },
  {
    path: 'lazy-loading',
    component: IntroductionComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
